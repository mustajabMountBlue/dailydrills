const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// Q1. Find all the items with price more than $65.
// Array of Object of Objects
const problem1 = (products) => {
  const result = [];

  products.forEach((product) => {
    const element = Object.keys(product).filter((productKey) => {
      let isPriceGreater = false;
      if (product[productKey] instanceof Array) {
        const recursion = problem1(product[productKey]);
        result.push(recursion);
      } else {
        isPriceGreater = +product[productKey].price.slice(1) > 65;
      }
      return isPriceGreater;
    });
    result.push(element);
  });
  return result;
};
const sol1 = problem1(products).flat(2);
console.log(sol1);

////////////////////////////////////////////////////////////////

// Q2. Find all the items where quantity ordered is more than 1.
const problem2 = (products) => {
  const result = [];

  products.forEach((product) => {
    const element = Object.keys(product).filter((productKey) => {
      let isQuantityGreater = false;
      if (product[productKey] instanceof Array) {
        const recursion = problem2(product[productKey]);
        result.push(recursion);
      } else {
        isQuantityGreater = product[productKey].quantity > 1;
      }
      return isQuantityGreater;
    });
    result.push(element);
  });
  return result;
};
const sol2 = problem2(products).flat(2);
console.log(sol2);

////////////////////////////////////////////////////////////////

// Q.3 Get all items which are mentioned as fragile.
const problem3 = (products) => {
  const result = [];

  products.forEach((product) => {
    const element = Object.keys(product).filter((productKey) => {
      let isPriceGreater = false;
      if (product[productKey] instanceof Array) {
        const recursion = problem3(product[productKey]);
        result.push(recursion);
      } else {
        isPriceGreater = product[productKey].type === "fragile";
      }
      return isPriceGreater;
    });
    result.push(element);
  });
  return result;
};
const sol3 = problem3(products).flat(2);
console.log(sol3);

////////////////////////////////////////////////////////////////

// Q.4 Find the least and the most expensive item for a single quantity.
const problem4 = (products) => {
  const result = products.reduce( (accu, curr) => {
    
  } )

  return result;
}
const sol4 = problem4(products).flat(2);
console.log(sol4);
