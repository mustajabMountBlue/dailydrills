/*
NOTE: Do not change the name of this file

NOTE: 
    - For all file operations use promises with fs. 
    - Promisify the fs callbacks rather than use fs/promises.
*/
const fs = require("fs");
const util = require("util");

const writeFile = util.promisify(fs.writeFile);
const unlink = util.promisify(fs.unlink);
const append = util.promisify(fs.appendFile);

// Q1. Create 2 files simultaneously (without chaining).
// Wait for 2 seconds and starts deleting them one after another. (in order)
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
function createFiles(data = "Dummy Data", numberOfFiles = 1, name) {
  return new Promise((resolve, reject) => {
    try {
      for (let i = 0; i < numberOfFiles; i++) {
        console.log(`Created ${name}_${i + 1}.txt of ${name}`);
        writeFile(`${name}_${i + 1}.txt`, data, (err) => {
          if (err) throw err;
        });
      }
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

function deleteFiles(name) {
  return new Promise((resolve, reject) => {
    try {
      for (let index = 0; index < 2; index++) {
        unlink(`${name}_${index + 1}.txt`, (err) => {
          console.log(`Deleted ${name}_${index + 1}.txt`);
          if (err) throw err;
        });
      }
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

createFiles("dummy input", 2, "problem1").then(() => {
  console.log("Waiting for 2 secs");
  return setTimeout(deleteFiles, 2 * 1000, "problem1");
});

/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/
const lipsumData =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.\nCurabitur eu accumsan turpis. Fusce finibus sem euismod massa fringilla, at maximus sem pulvinar. Proin sollicitudin porta leo sed pellentesque. Mauris nisi dolor, posuere in neque sit amet, sodales lobortis justo. Etiam venenatis metus aliquet purus porta, a aliquam nunc ullamcorper. Maecenas dapibus nibh non nunc facilisis, sit amet consequat lectus scelerisque. Pellentesque neque elit, sodales a commodo vel, vulputate in diam. Quisque quis lacus hendrerit, viverra dolor vel, volutpat nisi. Sed facilisis sollicitudin lorem, sit amet vulputate ipsum convallis sit amet. Praesent ipsum orci, vestibulum non consectetur id, molestie sit amet sem. Phasellus interdum purus id erat tempor rhoncus. In viverra quam et neque placerat malesuada. Donec interdum nulla non ante finibus, eget posuere sem ultricies.\nVestibulum tempor aliquam ipsum sit amet pretium. Quisque egestas risus nisi, vel pharetra arcu ultricies a. Quisque ornare nunc sit amet libero pharetra consectetur. Aenean ligula dui, faucibus vitae tempor in, molestie ut nibh. Donec finibus ex ex, et aliquet urna accumsan et. Duis felis nisi, tincidunt nec magna sed, tincidunt volutpat lectus. Mauris at gravida ipsum. Duis vel velit lectus. Etiam auctor fermentum tortor, eget condimentum elit. ";
createFiles(lipsumData, 1, "problem2");

// ====================================================

const activity = [
  "Login Success",
  "Login Failure",
  "GetData Success",
  "GetData Failure",
];

let loginStatus = activity[1];

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}
function writeLog(path, data) {
  return new Promise((resolve, reject) => {
    try {
      fs.appendFile(path, JSON.stringify(data) + "\n", (err) => {
        if (err) throw err;
      });
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}
function logData(user, activity) {
  // use promises and fs to save activity in some file

  return getData().then((data) => {
    if (data) activity += " GetData Success";
    else activity += " GetData Failure";
    const log = {
      user: user,
      timestamp: Date.now(),
      activity: activity,
    };

    return writeLog("log.txt", log).then(() => {
      if ((activity = "Login Success")) {
        return getData();
      } else {
        return Promise.reject(new Error("Login was failed"));
      }
    });
  });
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
*/

function login(user, val) {
  if (val % 2 === 0) {
    loginStatus = activity[0];
    return Promise.resolve(user);
  } else {
    loginStatus = activity[1];
    console.log(loginStatus);
    return Promise.reject(console.log("User not found"));
  }
}

login("user1", 4)
  .then(() => {
    return logData("user1", loginStatus);
  })
  .catch((err) => console.log("some error occured while logging"));
/*
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/
