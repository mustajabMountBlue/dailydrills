const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

// Movies with total earnings more than $500M
const movie500orGreater = Object.entries( favouritesMovies )
                            .filter( (movie) => parseInt(movie[1].totalEarnings.slice(1)) >= 500 );
console.log(movie500orGreater);



// Movies with total earnings more than $500M and Earn more than 3 oscarsNominations
const _3OscarNomineeAnd500orGreater = Object.entries( favouritesMovies )
                            .filter( (movie) => parseInt(movie[1].totalEarnings.slice(1)) >= 500 && movie[1].oscarNominations > 3 );
console.log(_3OscarNomineeAnd500orGreater);



// Movies of Leonardo
const leoDeMovies = Object.entries( favouritesMovies )
                            .filter( (movie) => movie[1].actors.includes('Leonardo Dicaprio') );
console.log(leoDeMovies);



// Sorting Movies Rating && income
const sortedMovies = Object.entries( favouritesMovies )
                            .sort( ( movie1, movie2 ) => ( movie2[1].imdbRating - movie1[1].imdbRating ) || ( parseInt(movie2[1].totalEarnings.slice(1)) - parseInt(movie1[1].totalEarnings.slice(1)) ) );
console.log(sortedMovies);



// Grouping on genre
function getMovieGenre( genreArr ) {
    // genre: ["drama", "crime"];
    let k = genreArr    
        .reduce( (accu, curr) => {
            const drama = curr.includes('drama') ? 'drama' : 0; 
            const scifi = curr.includes('sci-fi') ? 'scifi' : 0;
            const adventure = curr.includes('adventure') ? 'adventure' : 0;
            const thriller = curr.includes('thriller') ? 'thriller' : 0;
            const crime = curr.includes('crime') ? 'crime' : 0;
            
            if(!accu.hasOwnProperty('genre') ) accu['genre'] = drama || scifi || adventure || thriller || crime; 
            return accu;

        }, {} )
        console.log(k.genre)
        return k.genre
}

const movieGenre = Object.entries( favouritesMovies )
                            .reduce( (accu, curr) => {
                                
                                if( accu.hasOwnProperty(getMovieGenre(curr[1].genre)) ) {
                                    accu[getMovieGenre(curr[1].genre)].push(curr[0]);    
                                } else {
                                    accu[getMovieGenre(curr[1].genre)] = [ curr[0] ];
                                }
                                return accu;
                            }, {} );
console.log(movieGenre);