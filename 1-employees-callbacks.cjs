const fs = require("fs");
const path = require("path");
const dataPath = path.join(__dirname, "/data.json");
/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const data = {
  employees: [
    {
      id: 23,
      name: "Daphny",
      company: "Scooby Doo",
    },
    {
      id: 73,
      name: "Buttercup",
      company: "Powerpuff Brigade",
    },
    {
      id: 93,
      name: "Blossom",
      company: "Powerpuff Brigade",
    },
    {
      id: 13,
      name: "Fred",
      company: "Scooby Doo",
    },
    {
      id: 89,
      name: "Welma",
      company: "Scooby Doo",
    },
    {
      id: 92,
      name: "Charles Xavier",
      company: "X-Men",
    },
    {
      id: 94,
      name: "Bubbles",
      company: "Powerpuff Brigade",
    },
    {
      id: 2,
      name: "Xyclops",
      company: "X-Men",
    },
  ],
};
// Writing to File
fs.writeFile(dataPath, JSON.stringify(data), (err) => {
  if (err) console.error(err);
  else {
    console.log("File Wrote Successfully");
    console.log("Reading and Retrieving Started");
    fs.readFile(dataPath, "utf8", (err, data) => {
      if (err) {
        console.error(err);
      } else {
        // PROBLEMS CALLED HERE
        const dataOriginal = JSON.parse(data);
        const res1 = problem1(dataOriginal);
        console.log(res1);

        const res2 = problem2(dataOriginal);
        console.log(res2);

        const res3 = problem3(res2);
        console.log(res3);
      }
    });
  }
});

// 1. Retrieve data for ids : [2, 13, 23].
const problem1 = (data) =>
  Object.keys(data).reduce(
    (accu, keys) => {
      const employees = data[keys].filter(
        (employee) =>
          employee.id === 2 || employee.id === 13 || employee.id === 23
      );
      accu.filteredEmployees.push(employees);
      return accu;
    },
    { filteredEmployees: [] }
  );

// 2. Group data based on companies.
//  { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
const problem2 = (data) =>
  Object.keys(data).map((key) =>
    data[key].reduce((accu, curr) => {
      // data.keys => object
      if (accu.hasOwnProperty(curr.company)) {
        accu[curr.company] += curr;
      } else {
        accu[curr.company] = curr;
      }
      return accu;
    }, {})
  );

// 3. Get all data for company Powerpuff Brigade
const problem3 = (data) => data.map((team) => team["Powerpuff Brigade"]);
