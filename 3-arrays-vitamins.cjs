const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 
// AVAILABLE ITEMS
const available = items
                    .filter( (item) => (item.available) );
console.log(available);

///////////////////////////////////////////////////////////

// ONLY VITAMIN C
const onlyVitaminC = items
                    .filter( (item) => item.contains.toLowerCase() === 'vitamin c' );
console.log(onlyVitaminC);

///////////////////////////////////////////////////////////

// VITAMIN A ++
const vitaminA = items
                    .filter( (item) => item.contains.toLowerCase().includes('vitamin a') );
console.log(vitaminA);

///////////////////////////////////////////////////////////

// GROUPING
const groupByVitamins = items
                            .reduce( (accu, curr) => {
                                curr.contains.split(',').forEach( (vitamin) =>{
                                    if(accu.hasOwnProperty(vitamin.trim())) {
                                        accu[vitamin.trim()].push(curr.name);
                                    } else {
                                        accu[vitamin.trim()] = [ curr.name ];
                                    }
                                } )
                                return accu;
                            }, {} )
console.log(groupByVitamins);

///////////////////////////////////////////////////////////

// SORT ON NUMBER OF VITAMINS
const sortedOrder = items
                        .map( (item) => [ [item.contains.split(',').length], item ]  )
                        .sort( (itemA, itemB) => itemA[0] - itemB[0] );
console.log(sortedOrder);