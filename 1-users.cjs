const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

const videoGames = Object.entries(users)
                    .filter( (user) => {
                        const interest = user[1].interests || user[1].interest;
                        return interest[0].toLowerCase().includes('video games');

                    } )
                    .flat();
console.log( videoGames );
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

const germans = Object.entries(users)
                    .filter( (user) => user[1].nationality.toLowerCase() === 'germany' )
                    .flat();
console.log( germans );

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

const seniority = Object.entries(users)
                        .sort( (userA, userB) => {
                            let user1 = userA[1], user2 = userB[1];
                            if(user1.desgination.toLowerCase().includes('senior')) {
                                if(user2.desgination.toLowerCase().includes('senior')) 
                                    return user2.age - user1.age;
                                else 
                                    return -1;
                            }
                            else if(user2.desgination.toLowerCase().includes('senior')) return 1;
                            else if(user1.desgination.toLowerCase().includes('intern')) {
                                if(user2.desgination.toLowerCase().includes('intern')) 
                                    return user2.age - user1.age;
                                else 
                                    return 1;
                            }
                            else if(user2.desgination.toLowerCase().includes('intern')) return -1;
                            else return user2.age - user1.age;
                        } )
                        .flat();
console.log(seniority)

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

const masterDegree = Object.entries(users)
                            .filter( (user) => user[1].qualification.toLowerCase() === 'masters' )
                            .flat();
console.log(masterDegree);

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

const programmingLanguage = Object.entries(users)
                                .reduce( (accu, curr) => {
                                    curr[1].desgination.split(/[-\s]+/)
                                    .filter( str => str.toLowerCase() != 'senior' && str.toLowerCase() != 'developer' && str.toLowerCase() != 'intern' )
                                    .forEach( (lang) => {
                                        if(accu.hasOwnProperty(lang)) {
                                            accu[lang] += curr;
                                        } else {
                                            accu[lang] = curr;
                                        }
                                    } )        
                                    return accu;
                                }, {} );
console.log(programmingLanguage);

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////