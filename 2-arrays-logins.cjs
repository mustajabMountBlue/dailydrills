const log = [{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp","gender":"Male","ip_address":"253.171.63.171"},
{"id":2,"first_name":"Kenneth","last_name":"Hinemoor","email":"khinemoor1@yellowbook.com","gender":"Polygender","ip_address":"50.231.58.150"},
{"id":3,"first_name":"Roman","last_name":"Sedcole","email":"rsedcole2@addtoany.com","gender":"Genderqueer","ip_address":"236.52.184.83"},
{"id":4,"first_name":"Lind","last_name":"Ladyman","email":"lladyman3@wordpress.org","gender":"Male","ip_address":"118.12.213.144"},
{"id":5,"first_name":"Jocelyne","last_name":"Casse","email":"jcasse4@ehow.com","gender":"Agender","ip_address":"176.202.254.113"},
{"id":6,"first_name":"Stafford","last_name":"Dandy","email":"sdandy5@exblog.jp","gender":"Female","ip_address":"111.139.161.143"},
{"id":7,"first_name":"Jeramey","last_name":"Sweetsur","email":"jsweetsur6@youtube.com","gender":"Genderqueer","ip_address":"196.247.246.106"},
{"id":8,"first_name":"Anna-diane","last_name":"Wingar","email":"awingar7@auda.org.au","gender":"Agender","ip_address":"148.229.65.98"},
{"id":9,"first_name":"Cherianne","last_name":"Rantoul","email":"crantoul8@craigslist.org","gender":"Genderfluid","ip_address":"141.40.134.234"},
{"id":10,"first_name":"Nico","last_name":"Dunstall","email":"ndunstall9@technorati.com","gender":"Female","ip_address":"37.12.213.144"}]

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/
///////////////////////////////////////////////////////////////////////////////////

const agender = (log) => {
    const agender = log 
                    .filter( logInfo => logInfo.gender === 'Agender' );
    
    return agender;
}
console.log(agender(log));

///////////////////////////////////////////////////////////////////////////////////

const splitIP = (log) => {
    const ip = log
                .map( logInfo => logInfo.ip_address
                                        .split('.')
                                        .map( (eachBlock) => +eachBlock ) 
                    )
    return ip;
}
const ipArr = splitIP(log);
console.log(ipArr);

///////////////////////////////////////////////////////////////////////////////////

const secondComponentsSum = ( ipArr ) => {
    const res = ipArr
                    .reduce( (accu, curr) =>
                        accu + curr[1], 0 
                    );
    return res;
}
console.log(secondComponentsSum(ipArr));

///////////////////////////////////////////////////////////////////////////////////

const fourthComponentsSum = ( ipArr ) => {
    const res = ipArr
                    .reduce( (accu, curr) =>
                        accu + curr[3], 0 
                    );
    return res;
}
console.log(fourthComponentsSum(ipArr));

///////////////////////////////////////////////////////////////////////////////////

const fullName = (log) => {
    return log  
            .forEach( (logInfo) => 
                logInfo['full_name'] = `${logInfo.first_name} ${logInfo.last_name}` 
            )
}
fullName(log);
console.log(log)

///////////////////////////////////////////////////////////////////////////////////

const orgEmails = (log) => {
    const emails = log  
                    .filter( (logInfo) => logInfo.email.includes('org') )
                    .map( (info) => info.email );
    return emails;
}
console.log(orgEmails(log));

///////////////////////////////////////////////////////////////////////////////////

const orgAuComEmails = (log) => {
    const emails = log  
                    .filter( (logInfo) => logInfo.email.includes('org') || logInfo.email.includes('com') || logInfo.email.includes('au') )
                    .map( (logInfo) => logInfo.email )
                    .reduce( (accumulator, currentValue) => {
                        // Main Logic Goes Here
                        if(currentValue.includes('org')) {
                            if ( accumulator.hasOwnProperty('org') ) {
                                accumulator['org']++;
                            } else {
                                accumulator['org'] = 1;
                            }
                        }
                        if(currentValue.includes('au')) {
                            if ( accumulator.hasOwnProperty('au') ) {
                                accumulator['au']++;
                            } else {
                                accumulator['au'] = 1;
                            }
                        }
                        if(currentValue.includes('com')) {
                            if ( accumulator.hasOwnProperty('com') ) {
                                accumulator['com']++;
                            } else {
                                accumulator['com'] = 1;
                            }
                        }
                                
                        return accumulator;
                    }, {} // Passing Accumulator as empty object
                );
    return emails;
}
console.log(orgAuComEmails(log));

///////////////////////////////////////////////////////////////////////////////////

const sortDesFirst = (log) => {
    const res = log     
                .sort( (logA, logB) => {
                    const firstA = logA.first_name;
                    const firstB = logB.first_name;
                    if(firstA < firstB) return 1;
                    else if(firstA > firstB) return -1;
                    else return 0;
                } )
    return res;
}
console.log(sortDesFirst(log));