const cards = [
  {
    id: 1,
    card_number: "5602221055053843723",
    card_type: "china-unionpay",
    issue_date: "5/25/2021",
    salt: "x6ZHoS0t9vIU",
    phone: "339-555-5239",
  },
  {
    id: 2,
    card_number: "3547469136425635",
    card_type: "jcb",
    issue_date: "12/18/2021",
    salt: "FVOUIk",
    phone: "847-313-1289",
  },
  {
    id: 3,
    card_number: "5610480363247475108",
    card_type: "china-unionpay",
    issue_date: "5/7/2021",
    salt: "jBQThr",
    phone: "348-326-7873",
  },
  {
    id: 4,
    card_number: "374283660946674",
    card_type: "americanexpress",
    issue_date: "1/13/2021",
    salt: "n25JXsxzYr",
    phone: "599-331-8099",
  },
  {
    id: 5,
    card_number: "67090853951061268",
    card_type: "laser",
    issue_date: "3/18/2021",
    salt: "Yy5rjSJw",
    phone: "850-191-9906",
  },
  {
    id: 6,
    card_number: "560221984712769463",
    card_type: "china-unionpay",
    issue_date: "6/29/2021",
    salt: "VyyrJbUhV60",
    phone: "683-417-5044",
  },
  {
    id: 7,
    card_number: "3589433562357794",
    card_type: "jcb",
    issue_date: "11/16/2021",
    salt: "9M3zon",
    phone: "634-798-7829",
  },
  {
    id: 8,
    card_number: "5602255897698404",
    card_type: "china-unionpay",
    issue_date: "1/1/2021",
    salt: "YIMQMW",
    phone: "228-796-2347",
  },
  {
    id: 9,
    card_number: "3534352248361143",
    card_type: "jcb",
    issue_date: "4/28/2021",
    salt: "zj8NhPuUe4I",
    phone: "228-796-2347",
  },
  {
    id: 10,
    card_number: "4026933464803521",
    card_type: "visa-electron",
    issue_date: "10/1/2021",
    salt: "cAsGiHMFTPU",
    phone: "372-887-5974",
  },
];

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/
function sumEvenIndex(arr, n) {
  let i = 0,
    sum = 0;
  // calculating sum of even
  // number at even index
  for (i = 0; i < n; i += 2) {
    sum += +arr[i];
  }
  // required sum
  return sum;
}
const problem1 = (cards) => {
  const result = cards.filter(
    (card) => sumEvenIndex(card.card_number, card.card_number.length) % 2
  );
  return result;
};
// console.log(problem1(cards));

//////////////////////////////////////////////////////////////////////////////////

const problem2 = (cards) => {
  const month = cards.filter((card) => +card.issue_date.split("/")[0] <= 4);
  return month;
};
// console.log(problem2(cards));

//////////////////////////////////////////////////////////////////////////////////

const problem3 = (cards) => {
  cards.forEach((card) => {
    card["cvv"] = Math.floor(Math.random() * (999 - 100)) + 100; // [0, 1] * 999 = [0, 999]
  });
};
problem3(cards);
// console.log(cards);

//////////////////////////////////////////////////////////////////////////////////

const problem4 = (cards) => {
  cards.forEach((card, validity) => {
    const curr = 2022;
    const dateArray = card.issue_date.split("/");
    const [month, day, year] = [+dateArray[0], +dateArray[1], +dateArray[2]];
    // Valid
    card["isValid"] = curr - year < validity;
  });
};
problem4(cards, 3); // Valid for 3 Years only
// console.log(cards);

//////////////////////////////////////////////////////////////////////////////////

const problem5 = (cards) => {
  cards.forEach((card) => {
    const dateArray = card.issue_date.split("/");
    const [month, day, year] = [+dateArray[0], +dateArray[1], +dateArray[2]];
    card.isValid = month >= 4;
  });
  return cards;
};
problem5(cards);
// console.log(cards);

//////////////////////////////////////////////////////////////////////////////////

const problem6 = (cards) => {
  const sorted = [...cards].sort((cardA, cardB) => {
    let [monthA, dayA, yearA] = cardA.issue_date.split("/");
    let [monthB, dayB, yearB] = cardB.issue_date.split("/");

    return (
      +yearA - +yearB ||
      (+yearA == +yearB && +monthA - +monthB) ||
      (+yearA == +yearB && +monthA == +monthB && +dayA - +dayB)
    );
  });
  return sorted;
};
console.log(problem6(cards).map((card) => card.id));
console.log(cards.map((card) => card.id));

//////////////////////////////////////////////////////////////////////////////////

const problem7 = (cards) => {
  const grouped = cards.reduce((accu, curr) => {
    const month = +curr.issue_date.split("/")[0];
    if (accu.hasOwnProperty(month)) {
      accu[month].push(curr.card_number);
    } else {
      accu[month] = [curr.card_number];
    }
    return accu;
  }, {});
  return grouped;
};
// console.log(problem7(cards));
